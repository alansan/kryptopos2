//
//  TextTableViewCell.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 17/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    var cellTextField = UITextField(frame: CGRect(x: 20, y: 20, width: 100, height: 50))
    var maxLength = String()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCellTextField()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init recorder need to be implemented")
    }

    
    func configureCellTextField(){
        addSubview(cellTextField)

        cellTextField.placeholder = "Text Placeholder"
        
        cellTextField.translatesAutoresizingMaskIntoConstraints = false
        cellTextField.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        cellTextField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4).isActive = true
        cellTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        cellTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        cellTextField.delegate = self
    }
    
    func setData(payload : String?){
        cellTextField.text = payload
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= Int(maxLength) ?? 0
    }

   
    
    
}

