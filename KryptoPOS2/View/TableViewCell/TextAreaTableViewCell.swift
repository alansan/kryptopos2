//
//  TextAreaTableViewCell.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 17/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class TextAreaTableViewCell: UITableViewCell {

    var cellTextView = UITextView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureTextView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init recorder need to be implemented")
    }

    
    func configureTextView(){
        addSubview(cellTextView)

        cellTextView.text = "Textarea placeholder"
        cellTextView.translatesAutoresizingMaskIntoConstraints = false
        cellTextView.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        cellTextView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4).isActive = true
        cellTextView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        cellTextView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true

    }
    
    func setData(data : String?){
        cellTextView.text = data
    }
    

}
