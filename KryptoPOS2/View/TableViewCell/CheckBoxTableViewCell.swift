//
//  CheckBoxTableViewCell.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 18/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class CheckBoxTableViewCell: UITableViewCell {
    
    var cellSwitch = UISwitch(frame: CGRect(x: 20, y: 20, width: 100, height: 50))
    var labelView = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCellTextField()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init recorder need to be implemented")
    }
    
    
    func configureCellTextField(){
        addSubview(cellSwitch)
        addSubview(labelView)
        
        labelView.text = "No"
        
        labelView.translatesAutoresizingMaskIntoConstraints = false
        labelView.topAnchor.constraint(equalTo: topAnchor, constant: 25).isActive = true
        labelView.leadingAnchor.constraint(equalTo: cellSwitch.trailingAnchor, constant: 10).isActive = true
        labelView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true

        
        cellSwitch.addTarget(self, action: #selector(setup), for: .valueChanged)
    }

    
    @objc func setup(){
        if cellSwitch.isOn{
            labelView.text = "Yes"
        }else{
            labelView.text = "No"
        }
    }
    
    func setData(option : String?){
        if option == "Y"{
            cellSwitch.setOn(true, animated: false)
            labelView.text = "Yes"
        }else{
            cellSwitch.setOn(false, animated: false)
            labelView.text = "No"
        }
        
    }
    
    func getDataFromUser() -> String{
        if cellSwitch.isOn{
            return "Y"
        }else{
            return "N"
        }
    }
    
    
}
