//
//  DatePickerTableViewCell.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 17/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class DatePickerTableViewCell: UITableViewCell {

    var datePicker = UIDatePicker(frame: CGRect(x: 20, y: 20, width: 100, height: 50))
    
    var datePickerValue : String?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureTextView()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init recorder need to be implemented")
    }

    
    func configureTextView(){
        addSubview(datePicker)
        
        datePicker.datePickerMode = .dateAndTime
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        datePicker.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4).isActive = true
        datePicker.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        datePicker.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        datePicker.addTarget(self, action: #selector(getDateValue(datePicker:)), for: .valueChanged)
        getDateNow()
    }
    
    func getDateNow(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = formatter.string(from: date)
        datePickerValue = newDate
    }
    
    @objc func getDateValue(datePicker: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.string(from: datePicker.date)
        datePickerValue = date
        print(date)
    
    }
    
    func setCurrentDate(date: String){
        let dateStructed = date.prefix(19)
        let newDateStructed = dateStructed.replacingOccurrences(of: "T", with: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let date = dateFormatter.date(from: newDateStructed)

        datePicker.date = date!
    }

}


