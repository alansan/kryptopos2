//
//  PickerViewTableViewCell.swift
//  KryptoPOS
//
//  Created by Alan Santoso on 18/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class PickerViewTableViewCell: UITableViewCell {
    
    var pickerView = UIPickerView(frame: CGRect(x: 20, y: 20, width: 100, height: 40))
    var pickerData = [String]()
    
    var color : String = String()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configuresegmentedControl()
        setDefaultValue(item: "Green")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init recorder need to be implemented")
    }
    
    override func awakeFromNib() {
        pickerView.delegate = self
        pickerView.dataSource = self
        setDefaultValue(item: "Blue")

    }
    
    
    func configuresegmentedControl(){
        addSubview(pickerView)
        
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        pickerView.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        pickerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4).isActive = true
        pickerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        pickerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        
    }
    
    
    func setPickerViewData(payload : String?){
        let colorArray = payload?.split{$0 == "|"}.map(String.init)
        
        if let colors = colorArray {
            pickerData = colors
        }
        color = pickerData[0]
    }
    
    func setDefaultValue(item: String){
        if let indexPosition = pickerData.firstIndex(of: item){
            pickerView.selectRow(indexPosition, inComponent: 0, animated: true)
            pickerView.reloadComponent(0)
        }
        
    }
    
    
}

extension PickerViewTableViewCell : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        color = pickerData[row]
    }
    
   
    
    
}
