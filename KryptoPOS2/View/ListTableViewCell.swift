//
//  ListTableViewCell.swift
//  KryptoPOS2
//
//  Created by Alan Santoso on 23/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    
    
    let titleLabel = UILabel()
    let stackView = UIStackView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    func commonInit() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 2
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        
        titleLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        stackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    func setup(names: [String]) {
        stackView.arrangedSubviews.forEach { stackView.removeArrangedSubview($0);$0.removeFromSuperview() }
        names.forEach {
            let label = UILabel()
            label.text = $0
            stackView.addArrangedSubview(label)
        }
    }
    
    //    func setupView(count : Int, arrayString: [String]){
    //        var yHeight : CGFloat = 10
    //        for i in 0...count{
    //            var label = UILabel(frame: CGRect(x: 20, y: yHeight, width: 400, height: 20))
    //            print(arrayString)
    //            label.text = arrayString[i]
    //            addSubview(label)
    //            yHeight = yHeight + 20
    //        }
    //    }
    
    
}
