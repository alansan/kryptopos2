//
//  CrudModel.swift
//  KryptoPOS2
//
//  Created by Alan Santoso on 23/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import Foundation


// MARK: - CrudModel
struct CrudModel: Codable {
    let errorCode: Int?
    let errorMessage: String?
    let payload: [Payload]?
}

// MARK: - Payload
struct Payload: Codable {
    let crudId, crudName: String?
    let crudDescription: String?
    let crudColor, crudStatus, crudTimestamp: String?
    let crudSample: String?
}


// MARK: - Metadata
struct Metadata: Codable {
    let errorCode: Int?
    let errorMessage: String?
    let payload: [PayloadMetadata]?
}

// MARK: - PayloadMetadata
struct PayloadMetadata: Codable {
    var field, type, maxLength, input: String?
    var accepted: String?
    
    init(field:String, type:String, input:String, maxLength:String, accepted:String) {
        self.field = field
        self.input = input
        self.type = type
        self.maxLength = maxLength
        self.accepted = accepted
    }
}
