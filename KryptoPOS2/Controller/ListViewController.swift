//
//  ListViewController.swift
//  KryptoPOS2
//
//  Created by Alan Santoso on 23/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelView: UILabel!
    let api = APIClient()
    
    
    
    var dataMetadata = [[String:String]]()
    
    var dataList = [[String:String?]]()
    
    var dataToShow = [[String]]()
    
    var selectedRow = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        fetchMetadata2()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchMetadata2()
    }
    
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "addSegue", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editSegue"{
            if let vc = segue.destination as? ViewController {
                vc.dataMetadata = dataMetadata
                vc.dataList = dataList
                vc.getValue = dataToShow[selectedRow]
            }
        }else if segue.identifier == "addSegue"{
            if let vc = segue.destination as? AddViewController {
                vc.dataMetadata = dataMetadata
            }
        }
    }
    
    
    
}

extension ListViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListTableViewCell
        cell.setup(names: dataToShow[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRow = indexPath.row
        performSegue(withIdentifier: "editSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete){
            if let id = dataList[indexPath.row]["crud_id"]! {
                print(id)
                deleteFromList(id: id)
            }
        }
    }
    
    func deleteFromList(id:String){
        api.deleteFromList(id: id) { (res) in
            switch res {
            case .success(let _):
                self.deleteAlert()
                self.fetchMetadata2()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func deleteAlert(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: "Delete Data Success", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                print("Ok Delete")
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}

// MARK: - Networking
extension ListViewController {
    
    func fetchMetadata2(){
        api.checkoutPayload2 { (result) in
            switch result{
            case .success(let metadata):
                self.dataMetadata = metadata["payload"] as! Array<[String:String]>
                //                print(self.dataMetadata)
                self.fetchList2()
            case .failure(let error):
                print("error")
            }
        }
    }
    
    
    func fetchList2(){
        api.readList2 { (result) in
            switch result {
            case .success(let dictData):
                self.dataList = dictData["payload"] as! Array<[String:String?]>
                self.setupUI()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func setupUI(){
        self.dataToShow.removeAll()
        self.dataList.forEach { (data) in
            var string : [String]? = [String]()
            self.dataMetadata.forEach { (insideData) in
                let hasilSatuan = data[insideData["field"]!]!
                string?.append(hasilSatuan ?? "null")
            }
            self.dataToShow.append(string!)
        }
        DispatchQueue.main.async {
            print("reloaddd")
            self.tableView.reloadData()
        }
    }
    
    
    
}

