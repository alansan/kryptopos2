//
//  AddViewController.swift
//  KryptoPOS2
//
//  Created by Alan Santoso on 25/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataMetadata = [[String:String]]()
    
    var hasil : [String] = [String]()
    
    let api = APIClient()
    
    struct Cells{
        static let textCell = "textCell"
        static let textAreaCell = "textAreaCell"
        static let datePickerCell = "datePickerCell"
        static let comboboxCell = "comboboxCell"
        static let checkboxCell = "checkboxCell"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        
        for index in 0..<dataMetadata.count{
            switch dataMetadata[index]["input"] {
            case "text":
                let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! TextTableViewCell
                let value = cell.cellTextField.text ?? "null"
                hasil.append(value)
            case "textarea":
                let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! TextAreaTableViewCell
                let value = cell.cellTextView.text ?? "null"

                hasil.append(value)
            case "datepicker":
                let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! DatePickerTableViewCell
                hasil.append(cell.datePickerValue ?? "null")
            case "checkbox":
                let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! CheckBoxTableViewCell
                let value = cell.getDataFromUser()
                hasil.append(value ?? "null")
            case "combobox":
                let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! PickerViewTableViewCell
                hasil.append(cell.color ?? "null")

            default:
                print("nothing")
            }
        }
        print(hasil)
        saveData()
        
    }
    
    
    
}

// MARK: - NETWORKING
extension AddViewController{
    func saveData(){
        var keys = [String]()
        var values :[String] = [String]()
        
        //GET KEYS
        dataMetadata.forEach { (data) in
            keys.append(data["field"]!)
        }
        
        //GET VALUES
        values = hasil
        
        //CREATE DICTIONARY (KEYS, Values)
        let fullStack = Dictionary(uniqueKeysWithValues: zip(keys, values))
        print(fullStack)
        
        //SAVE TO DATABASE
        api.addToList(parameter: fullStack) { (result) in
            switch result{
            case .success(let data):
                self.AddAlert()
            case .failure(let err):
                print(err)
            }
        }
        
    }
    
    func AddAlert(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: "Add Data Success", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}



// MARK: - TABLE VIEW
extension AddViewController : UITableViewDataSource, UITableViewDelegate{
    
    func registerCell() {
        tableView.register(TextTableViewCell.self, forCellReuseIdentifier: Cells.textCell)
        tableView.register(TextAreaTableViewCell.self, forCellReuseIdentifier: Cells.textAreaCell)
        tableView.register(DatePickerTableViewCell.self, forCellReuseIdentifier: Cells.datePickerCell)
        tableView.register(PickerViewTableViewCell.self, forCellReuseIdentifier: Cells.comboboxCell)
        tableView.register(CheckBoxTableViewCell.self, forCellReuseIdentifier: Cells.checkboxCell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataMetadata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let payload = dataMetadata[indexPath.row]["input"]{

            switch payload {
            case "text":
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.textCell, for: indexPath) as? TextTableViewCell)!
                cell.maxLength = dataMetadata[indexPath.row]["maxLength"] ?? "255"
                return cell
                
            case "textarea":
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.textAreaCell, for: indexPath) as? TextAreaTableViewCell)!
                return cell
                
            case "datepicker":
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.datePickerCell, for: indexPath) as? DatePickerTableViewCell)!
                cell.datePicker.datePickerMode = .dateAndTime
                return cell
                
            case "checkbox":
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.checkboxCell, for: indexPath) as? CheckBoxTableViewCell)!
                
                return cell
                
            case "combobox":
                let cell = tableView.dequeueReusableCell(withIdentifier: Cells.comboboxCell, for: indexPath) as! PickerViewTableViewCell
                cell.setPickerViewData(payload: dataMetadata[indexPath.row]["accepted"])
                return cell
                
            default:
                let cell = (tableView.dequeueReusableCell(withIdentifier: Cells.textCell, for: indexPath) as? TextTableViewCell)!
                return cell
                
            }
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}
