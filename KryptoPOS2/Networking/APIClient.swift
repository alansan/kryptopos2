//
//  APIClient.swift
//  KryptoPOS2
//
//  Created by Alan Santoso on 23/01/20.
//  Copyright © 2020 Alan Santoso. All rights reserved.
//

import Foundation
import UIKit


enum APIError : Error {
    case responseProblem
    case decodingProblem
    case otherProblem
}

class APIClient{
    
    let resourceURL: URL
    
    init(){
        let resourceString = "https://us-central1-shopick-io-cvm.cloudfunctions.net/crud"
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
        
        self.resourceURL = resourceURL
    }
    
    func checkoutPayload(completion: @escaping(Result<Metadata,APIError>) -> ()){
        let parameters = ["a": "metadata",
                          "p": "{}"] as [String : Any]
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let json = try decoder.decode(Metadata.self, from: jsonData)
                completion(.success(json))
            } catch {
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
    }
    
    
    func checkoutPayload2(completion: @escaping(Result<[String:Any],APIError>) -> ()){
        var payloadData = [String:Any]()
        
        let parameters = ["a": "metadata",
                          "p": "{}"] as [String : Any]
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                payloadData = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String: Any]
                completion(.success(payloadData))
            } catch {
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
    }
    
    func readList2(completion: @escaping(Result<[String:Any],APIError>) -> ()){
        var payloadData = [String:Any]()
        
        
        let parameters = ["a": "list",
                          "p": "{}"] as [String : Any]
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                payloadData = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String: Any]
                
                completion(.success(payloadData))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
    }
    
    func readList(completion: @escaping(Result<CrudModel,APIError>) -> ()){
        let parameters = ["a": "list",
                          "p": "{}"] as [String : Any]
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let json = try decoder.decode(CrudModel.self, from: jsonData)
                completion(.success(json))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
    }
    
    
    
    func addToList(parameter: [String:String] ,completion: @escaping(Result<[String:Any],APIError>) -> ()){
        
        var parameters = ["a": "insert",
                          "p": "test"
            ] as [String:Any]
        
        parameters["p"] = parameter
        
        print(parameters)
        
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any]
                completion(.success(json!))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
        
    }
    
    
    func deleteFromList(id :String ,completion: @escaping(Result<[String:Any],APIError>) -> ()){
        
        let parameters = ["a" : "delete",
                          "p":
                            [ "crud_id" : "\(id)"]
            ] as [String : Any]
        
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any]
                completion(.success(json!))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
        
    }
    
    func updateList(parameter :[String:String] ,completion: @escaping(Result<[String:Any],APIError>) -> ()){
        var parameters = ["a": "update",
                          "p": "test"
            ] as [String:Any]
        
        parameters["p"] = parameter
        
        var urlRequest = URLRequest(url: resourceURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.otherProblem))
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any]
                completion(.success(json!))
            } catch{
                completion(.failure(.decodingProblem))
            }
        })
        task.resume()
        
        
    }
    
}
